\contentsline {chapter}{HALAMAN JUDUL}{i}{section*.2}
\contentsline {chapter}{LEMBAR PERSETUJUAN}{ii}{section*.4}
\contentsline {chapter}{LEMBAR PERNYATAAN ORISINALITAS}{iii}{section*.5}
\contentsline {chapter}{LEMBAR PENGESAHAN}{iv}{section*.6}
\contentsline {chapter}{\uppercase {Kata Pengantar}}{v}{section*.7}
\contentsline {chapter}{LEMBAR PERSETUJUAN PUBLIKASI ILMIAH}{vi}{section*.8}
\contentsline {chapter}{ABSTRAK}{vii}{section*.9}
\contentsline {chapter}{Daftar Isi}{ix}{section*.10}
\contentsline {chapter}{Daftar Gambar}{xiv}{section*.11}
\contentsline {chapter}{Daftar Tabel}{xv}{section*.12}
\contentsline {chapter}{\numberline {1}\uppercase {Pendahuluan}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Pertanyaan Penelitian}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Tujuan dan Manfaat Penelitian}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Ruang Lingkup Penelitian}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Sistematika Penulisan}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}\uppercase {Landasan Teori}}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Artikel Ilmiah}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}{\textit {Proofread}}}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Kesalahan pada Penulisan Artikel Ilmiah}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Aturan Penulisan Ilmiah Bahasa Indonesia}{7}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Pedoman Umum Ejaan Bahasa Indonesia}{7}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Tata Bahasa Baku Bahasa Indonesia}{9}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}{\textit {Part of Speech Tagging}} (POSTAG)}{9}{section.2.5}
\contentsline {section}{\numberline {2.6}{\textit {Named Entity Recognition}} (NER)}{10}{section.2.6}
\contentsline {section}{\numberline {2.7}Model \textit {Machine Learning}}{11}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Naive Bayes}{12}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}\textit {Sequence Model}}{12}{subsection.2.7.2}
\contentsline {subsubsection}{\numberline {2.7.2.1}Maximum Entropy}{12}{subsubsection.2.7.2.1}
\contentsline {section}{\numberline {2.8}PDFMiner}{13}{section.2.8}
\contentsline {section}{\numberline {2.9}\textit {Natural Language Toolkit}}{14}{section.2.9}
\contentsline {section}{\numberline {2.10}Scikit-learn}{14}{section.2.10}
\contentsline {chapter}{\numberline {3}\uppercase {Metodologi Penelitian}}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Penentuan Kesalahan}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}{Kesalahan Ejaan}}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Kesalahan Tipografi}{15}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Kesalahan Penggunaan Kata}{16}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}{Kesalahan Bibliografi}}{16}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Kesalahan {\textit {Layouting}}}{16}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Rancangan Pembentukan Sistem}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Identifikasi Kesalahan Penulisan Ilmiah dan Strategi Pendekatannya}{19}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Pengumpulan Aturan}{19}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Identifikasi Kesalahan yang Melanggar Aturan}{19}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Kesalahan Tipografi}{19}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}Kesalahan Ejaan}{25}{subsubsection.3.3.2.2}
\contentsline {subsection}{\numberline {3.3.3}Strategi Pendeteksian Kesalahan}{28}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}Penggunaan \textit {Rule Base}}{28}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}Penggunaan NER}{29}{subsubsection.3.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3.3}Penggunaan POSTAG}{29}{subsubsection.3.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.4}Penggunaan Klasifikasi}{30}{subsubsection.3.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.3.5}Pengelompokan Aturan Berdasarkan Strategi}{30}{subsubsection.3.3.3.5}
\contentsline {section}{\numberline {3.4}Pengumpulan dan \textit {Parsing} Data}{31}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Pengumpulan Data dari Laporan Kerja Praktik (KP) Fasilkom}{31}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Pengumpulan Data dari Laporan Tugas Akhir S1 Fasilkom}{32}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}\textit {Parsing} Dokumen}{32}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}Pembubuhan POSTAG dan NER}{33}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Pembubuhan POSTAG}{34}{subsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.1.1}Evaluasi POSTAG}{34}{subsubsection.3.5.1.1}
\contentsline {subsection}{\numberline {3.5.2}Pembangunan NER}{34}{subsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.2.1}Pembubuhan POSTAG}{35}{subsubsection.3.5.2.1}
\contentsline {subsubsection}{\numberline {3.5.2.2}Ekstraksi Fitur}{35}{subsubsection.3.5.2.2}
\contentsline {subsubsection}{\numberline {3.5.2.3}Pelatihan NER}{36}{subsubsection.3.5.2.3}
\contentsline {subsubsection}{\numberline {3.5.2.4}Evaluasi NER}{37}{subsubsection.3.5.2.4}
\contentsline {section}{\numberline {3.6}Pembentukan Kamus}{37}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Kamus Hari dan Bulan}{37}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Kamus Terminologi Agama}{37}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Kamus Nama Negara}{38}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}Kamus Kata Tugas}{38}{subsection.3.6.4}
\contentsline {subsection}{\numberline {3.6.5}Kamus Gelar}{38}{subsection.3.6.5}
\contentsline {section}{\numberline {3.7}Pendeteksian Kesalahan}{38}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Kesalahan Huruf Kapital}{39}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Kesalahan Huruf Miring}{41}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Kesalahan Penggunaan Titik}{42}{subsection.3.7.3}
\contentsline {subsection}{\numberline {3.7.4}Kesalahan Penggunaan Koma}{43}{subsection.3.7.4}
\contentsline {subsection}{\numberline {3.7.5}Alur Pendeteksian Kesalahan}{45}{subsection.3.7.5}
\contentsline {section}{\numberline {3.8}Pengoreksian Kesalahan}{50}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Pengoreksian Huruf Kapital}{50}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Pengoreksian Huruf Miring}{52}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Pengoresian Tanda Baca Titik}{53}{subsection.3.8.3}
\contentsline {subsection}{\numberline {3.8.4}Pengoreksian Tanda Baca Koma}{53}{subsection.3.8.4}
\contentsline {section}{\numberline {3.9}Perancangan Cara Kerja Sistem}{55}{section.3.9}
\contentsline {chapter}{\numberline {4}\uppercase {Implementasi}}{58}{chapter.4}
\contentsline {section}{\numberline {4.1}Pembubuhan POSTAG}{58}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}POSTAG Rashel, dkk yang Menggunakan \textit {Rule Base}}{58}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}POSTAG Rashel, dkk dalam Bentuk CRF}{61}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Pembangunan NER}{63}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Ekstraksi Fitur}{63}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Pelatihan Fitur}{64}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}Pelatihan Fitur dengan Naive Bayes Menggunakan NLTK}{65}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}Pelatihan Fitur dengan Maximum Entropy Menggunakan NLTK}{66}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.3}Klasifikasi Entitas Nama pada NLTK}{66}{subsubsection.4.2.2.3}
\contentsline {subsubsection}{\numberline {4.2.2.4}Pelatihan Fitur dengan Maximum Entropy Menggunakan Scikit-Learn}{67}{subsubsection.4.2.2.4}
\contentsline {subsubsection}{\numberline {4.2.2.5}Klasifikasi Entitas Nama dengan Scikit-Learn}{69}{subsubsection.4.2.2.5}
\contentsline {section}{\numberline {4.3}Pendeteksian Kesalahan}{70}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Pendeteksian Kesalahan Huruf Kapital}{70}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}Huruf Kapital di Awal Kalimat}{70}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}Kesalahan Huruf Kapital untuk Nama Hari dan Bulan}{71}{subsubsection.4.3.1.2}
\contentsline {subsubsection}{\numberline {4.3.1.3}Kesalahan Huruf Kapital untuk Nama Geografi Berupa Negara}{71}{subsubsection.4.3.1.3}
\contentsline {subsubsection}{\numberline {4.3.1.4}Kesalahan Huruf Kapital untuk Terminologi Agama}{72}{subsubsection.4.3.1.4}
\contentsline {subsubsection}{\numberline {4.3.1.5}Kesalahan Huruf Kapital untuk Kata Tugas}{73}{subsubsection.4.3.1.5}
\contentsline {subsubsection}{\numberline {4.3.1.6}Kesalahan Huruf Kapital Menggunakan NER}{74}{subsubsection.4.3.1.6}
\contentsline {subsection}{\numberline {4.3.2}Pendeteksian Kesalahan Huruf Cetak Miring}{75}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}Huruf Cetak Miring untuk Kata Berbahasa Asing}{75}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}Huruf Tidak Dicetak Miring untuk Kata Berbahasa Asing yang Merupakan Nama}{76}{subsubsection.4.3.2.2}
\contentsline {subsection}{\numberline {4.3.3}Pendeteksian Kesalahan Tanda Baca Titik}{77}{subsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.1}Titik pada Angka Ribuan}{77}{subsubsection.4.3.3.1}
\contentsline {subsection}{\numberline {4.3.4}Pendeteksian Kesalahan Tanda Baca Koma}{79}{subsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.4.1}Koma untuk Kata Penghubung pada Kalimat Setara}{79}{subsubsection.4.3.4.1}
\contentsline {subsubsection}{\numberline {4.3.4.2}Koma untuk Kata Penghubung Antarkalimat}{80}{subsubsection.4.3.4.2}
\contentsline {subsubsection}{\numberline {4.3.4.3}Koma di antara Anak Kalimat dan Induk Kalimat}{80}{subsubsection.4.3.4.3}
\contentsline {subsubsection}{\numberline {4.3.4.4}Koma di antara Nama dan Gelar}{81}{subsubsection.4.3.4.4}
\contentsline {section}{\numberline {4.4}Pengoreksian Kesalahan}{82}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Pengoreksi Kesalahan Huruf Kapital}{82}{subsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.1.1}Huruf Kapital di Awal Kalimat}{83}{subsubsection.4.4.1.1}
\contentsline {subsubsection}{\numberline {4.4.1.2}Huruf Kapital pada Kata Tugas}{84}{subsubsection.4.4.1.2}
\contentsline {subsubsection}{\numberline {4.4.1.3}Huruf Kapital pada Nama Negara, Hari dan Bulan, Terminologi Agama, dan Entitas Nama}{85}{subsubsection.4.4.1.3}
\contentsline {subsection}{\numberline {4.4.2}Pengoreksi Kesalahan Huruf Miring}{86}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}Huruf Miring pada Kata Bahasa Asing}{86}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}Huruf Tidak Miring pada Kata Bahasa Asing yang Merupakan Nama}{86}{subsubsection.4.4.2.2}
\contentsline {subsection}{\numberline {4.4.3}Pengoreksi Kesalahan Tanda Baca Titik}{87}{subsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.3.1}Kesalahan Tanda Baca Titik pada Bilangan Ribuan}{87}{subsubsection.4.4.3.1}
\contentsline {subsection}{\numberline {4.4.4}Pengoreksi Kesalahan Tanda Baca Koma}{88}{subsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.4.1}Tanda Koma pada Kalimat Setara dan di antara Nama dan Gelar}{88}{subsubsection.4.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.4.2}Tanda Koma setelah Kata Hubung Antarkalimat}{89}{subsubsection.4.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.4.3}Tanda Baca Koma pada Anak Kalimat yang Mendahului Induk Kalimat}{90}{subsubsection.4.4.4.3}
\contentsline {chapter}{\numberline {5}\uppercase {Evaluasi dan Analisis Hasil}}{92}{chapter.5}
\contentsline {section}{\numberline {5.1}Pengumpulan dan Pengerucutan Aturan}{92}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Pengumpulan Aturan}{92}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Pengerucutan Aturan}{93}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Hasil POSTAG}{95}{section.5.2}
\contentsline {section}{\numberline {5.3}Pendeteksian Kesalahan}{96}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Tanda Baca dan Tipografi}{96}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}Evaluasi POSTAG dan NER}{97}{subsubsection.5.3.1.1}
\contentsline {subsubsection}{\numberline {5.3.1.2}Evaluasi Regex \textit {Base} dan \textit {Dictionary Base}}{102}{subsubsection.5.3.1.2}
\contentsline {subsection}{\numberline {5.3.2}Evaluasi Pemeriksaan Tanda Baca Titik}{103}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Aturan yang Tidak Dievaluasi}{104}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Variabel Akhir}{104}{subsection.5.3.4}
\contentsline {section}{\numberline {5.4}Pengoreksian Kesalahan}{106}{section.5.4}
\contentsline {chapter}{\numberline {6}\uppercase {Penutup}}{108}{chapter.6}
\contentsline {section}{\numberline {6.1}Kesimpulan}{108}{section.6.1}
\contentsline {section}{\numberline {6.2}Saran}{109}{section.6.2}
\contentsline {chapter}{Daftar Referensi}{111}{section*.54}
\contentsline {chapter}{LAMPIRAN}{1}{section*.55}
\contentsline {chapter}{Lampiran 1}{2}{section*.56}
